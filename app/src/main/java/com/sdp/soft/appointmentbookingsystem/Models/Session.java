package com.sdp.soft.appointmentbookingsystem.Models;

import java.util.Date;

public class Session {
    private  int sessionId;
    private String startTime;
    private String finishTime;
    private String sessionDate;

    public Session(String startTime, String finishTime, int sessionId){
        this.startTime = startTime;
        this.finishTime = finishTime;
        this.sessionId = sessionId;
        this.sessionDate = "";
    }

    public void setSessionId(int sessionId){
        this.sessionId = sessionId;
    }

    public int getSessionId(){
        return sessionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public String sessionStartFinishTimes(){
        return "From " + this.getStartTime() + " to " + this.getFinishTime();
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }
}