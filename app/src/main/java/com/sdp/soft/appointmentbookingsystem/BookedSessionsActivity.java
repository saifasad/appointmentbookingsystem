package com.sdp.soft.appointmentbookingsystem;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.sdp.soft.appointmentbookingsystem.Controllers.NetworkController;
import com.sdp.soft.appointmentbookingsystem.Models.Coach;
import com.sdp.soft.appointmentbookingsystem.Models.Session;
import com.sdp.soft.appointmentbookingsystem.Utls.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.SocketPermission;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This Activity displays a list of sessions that have been previously booked
 * by the user.
 * It also allows for cancellation of a booked session by clicking on the session
 * from the list and choosing delete from the dialog box displayed.
 */
public class BookedSessionsActivity extends AppCompatActivity {

    private Map<Coach, Session> mBookedSessions;
    private ListView mBookedSessionsListView;
    private BookedSessionAdapter mBookedSessionAdapter;
    private TextView noBookedSessionsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_sessions);

        initViews();

        mBookedSessions = new HashMap<Coach, Session>(3);
        mBookedSessionAdapter = new BookedSessionAdapter(mBookedSessions);
        mBookedSessionsListView = (ListView) findViewById(R.id.booked_sessions_list);
        mBookedSessionsListView.setAdapter(mBookedSessionAdapter);
        mBookedSessionsListView.setFastScrollEnabled(true);

        //A click listener for the list of sessions, it displays a dialog box that allows the user
        //to delete a speicif session from the list (i.e cancel a booked session)
        mBookedSessionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                //get the session id from the list and send it to the server to be deleted/cancelled
                Map.Entry<Coach, Session> listItemObject = mBookedSessionAdapter.getItem(position);
                Session session = listItemObject.getValue();
                cancelBooking(session.getSessionId());
            }
        });

        getAllSessionsForClient();
    }

    /**
     * Delete/Cancel a specific booked session by sending the session id to the server.
     * @param sessionId: the id of the session to be deleted.
     */
    private void cancelBooking(final int sessionId) {
        //create a dialog box asking the user to confirm cancellation
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("cancel session");
        builder.setMessage("do you want cancel this session?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                // Tag used to cancel the request
                String tag_string_req = "string_req";

                String url = ("http://autsdp2016.com/cancelsession.php?sessionid="+sessionId);

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                String cancelSessionResponse = response.trim();

                                if(cancelSessionResponse.equals("0")) {
                                    Toast.makeText(BookedSessionsActivity.this,
                                            "Error Cancelling Session", Toast.LENGTH_SHORT).show();
                                } else if (cancelSessionResponse.equals("1")) {
                                    Log.d("Success ", response.toString());
                                    //display a toast to confirm success/failure
                                    Toast.makeText(BookedSessionsActivity.this,
                                            "Session has been cancelled", Toast.LENGTH_SHORT).show();
                                    //delete a specific session
                                    mBookedSessionAdapter.removeSessionFromList(sessionId);

                                    //if all the sessions in the list have been canceked
                                    //display a message to indicate that there are no more
                                    //sessions available.
                                    if (mBookedSessionAdapter.getCount() == 0) {
                                        noBookedSessionsTextView.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                VolleyLog.d("Error: " + error.getMessage());
                            }
                        });
                dialog.dismiss();
                // Adding request to request queue
                NetworkController.getInstance().addToRequestQueue(stringRequest, tag_string_req);
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if cancel is selected nothing happens
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    private void initViews(){
        noBookedSessionsTextView = (TextView) findViewById(R.id.no_booked_sessions_text_view);
    }

    private void getAllSessionsForClient (){
        //retrieve user id from shared prefs
        //this will be appended to the url
        String userId = getSharedPreferences("loginPrefs", MODE_PRIVATE).getString("userId", "0");

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Fetching Booked Sessions...");
        pDialog.show();

        // Tag used to cancel the request
        String tag_json_arry = "json_array_req";

        String url = "http://autsdp2016.com/usersessions.php?userid="+userId;

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Success ", response.toString());
                        pDialog.dismiss();
                        mBookedSessions.clear();

                        if(response.length() == 0) {
                            //No sessions have been booked.
                            noBookedSessionsTextView.setVisibility(View.VISIBLE);
                        } else {
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject responseObject = response.getJSONObject(i);

                                    Session session = StringUtils.extractSessionDetailsFromJsonObject(responseObject);
                                    //String coachName = responseObject.getString("coachname");
                                    Coach coach = new Coach(responseObject.getString("coachname"), responseObject.getString("coachname"), Integer.parseInt(responseObject.getString("coachid")));
                                    mBookedSessions.put(coach, session);
                                }
                                mBookedSessionAdapter.updateAdapterData(mBookedSessions);
                                mBookedSessionAdapter.notifyDataSetChanged();

                                //remove the "no bookings" message if it is displayed
                                noBookedSessionsTextView.setVisibility(View.GONE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d("Error: " + error.getMessage());
                        pDialog.dismiss();
                }
        });
        // Adding request to request queue
        NetworkController.getInstance().addToRequestQueue(req, tag_json_arry);
    }

    /**
     * A custome adapter class for the mBookedSessionsListView ListView.
     * It acts as a datasource for the list items in the ListView
     * and provides a custom view for each list item.
     */
    private class BookedSessionAdapter extends BaseAdapter {
        private ArrayList mBookedSessionsList;

        public BookedSessionAdapter(Map<Coach, Session> mBookedSessionsMap) {
            mBookedSessionsList = new ArrayList();
            mBookedSessionsList.addAll(mBookedSessionsMap.entrySet());
        }

        public void removeSessionFromList(int sessionId) {
            Iterator bookedSessionsIterator = mBookedSessions.entrySet().iterator();
            while (bookedSessionsIterator.hasNext()) {
                Map.Entry<Coach, Session> entry = (Map.Entry<Coach, Session>) bookedSessionsIterator.next();
                Session value = entry.getValue();
                if (value.getSessionId() == sessionId) {
                    mBookedSessions.remove(entry.getKey());
                    updateAdapterData(mBookedSessions);
                    break;
                }
            }

        }

        public void updateAdapterData(Map<Coach, Session> mBookedSessionsMap) {
            mBookedSessionsList = new ArrayList();
            mBookedSessionsList.addAll(mBookedSessionsMap.entrySet());
            this.notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mBookedSessions.size();
        }

        @Override
        public Map.Entry<Coach, Session> getItem(int position) {
            return (Map.Entry) mBookedSessionsList.get(position);
        }

        //not needed in this context, it must exist, since it is defined in
        //the BaseAdapter class
        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            //set the layout of each list item to the custom layout "booked_session_list_item"
            View result = LayoutInflater.from(parent.getContext()).inflate(R.layout.booked_session_list_item, parent, false);
            Map.Entry<Coach, Session> item = getItem(position);

            String sessionDetails = StringUtils.getWeekdayName(item.getValue().getSessionDate()) + " " +
                    item.getValue().getSessionDate() + "\n" + item.getValue().sessionStartFinishTimes();
            String coachDetails = "With " + item.getKey().getFullName();

            TextView sessionTextView = (TextView) result.findViewById(R.id.text1);
            TextView coachTextView = (TextView) result.findViewById(R.id.text2);

            sessionTextView.setText(sessionDetails);
            coachTextView.setText(coachDetails);

            return result;
        }
    }
}