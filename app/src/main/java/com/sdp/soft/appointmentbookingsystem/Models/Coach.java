package com.sdp.soft.appointmentbookingsystem.Models;

public class Coach {
    private int coachId;
    private String firstName;
    private String lastName;

    public Coach(String firstName, String lastName, int id){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setCoachId(id);
    }

    public int getCoachId() {
        return coachId;
    }

    public void setCoachId(int coachId) {
        this.coachId = coachId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName (){
        return this.getFirstName() + " " + this.getLastName();
    }
}