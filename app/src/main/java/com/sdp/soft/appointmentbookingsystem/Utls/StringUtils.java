package com.sdp.soft.appointmentbookingsystem.Utls;

import com.sdp.soft.appointmentbookingsystem.Models.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Contains utility methods for string processing.
 */
public class StringUtils {

    /**
     * This method removes unwanted brackets form the date string and changes
     * the format of the date from dd-mm-yyyy to yyyy-mm-dd
     * @param date: a string that represents a date of the format {dd-mm-yyyy}
     * @return: a string that represents a date of the format yyyy-mm-dd
     */
    public static String exractDateFromString(String date){

        date = date.substring(date.indexOf('{')+1, date.indexOf('}'));

        String [] dateParts = date.split("-");
        String year = dateParts[0];

        //a temporary hack to overpass the issue with the calendar returning a month before
        // the selected month.
        Integer monthNum = Integer.parseInt(dateParts[1]) + 1;
        String month = monthNum.toString();

        String day = dateParts[2];

        date = year + '-' + month + '-' + day;
        return date;
    }

    /**
     * Retrieve the session details from a JSON object that represents a session.
     * @param response a JSON object that represents a session retreived from the server.
     * @return a session object with the details contained in the response object.
     */
    public static Session extractSessionDetailsFromJsonObject (JSONObject response) {

        int sessionId = 0;
        String sessionStartTime = "";
        String sessionFinishTime = "";
        String sessionDate = "";

        try {
            sessionId = Integer.parseInt(response.getString("session"));
            sessionStartTime = response.getString("time").substring(0, response.getString("time").indexOf('-'));
            sessionFinishTime = response.getString("time").substring(response.getString("time").indexOf('-') + 1, response.getString("time").length());
            sessionDate = response.getString("date");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Session session = new Session(sessionStartTime, sessionFinishTime, sessionId);
        session.setSessionDate(sessionDate);

        return session;
    }

    /**
     * This method takes a string as input which represents a date and returns the name of the
     * weekday represented by that date.
     * @param input a date of the format "yyyy-mm-dd"
     * @return weekday name (Monday, Tuesday,....)
     */
    public  static String getWeekdayName (String input) {
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        String weekDay = "";
        SimpleDateFormat outFormat = null;

        try {
            date = inFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) {
            weekDay = "";
        } else {
            outFormat = new SimpleDateFormat("EEEE");
            weekDay = outFormat.format(date);
        }

        return weekDay;
    }
}
