package com.sdp.soft.appointmentbookingsystem;

import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This Activity allows the user to reset the acocunt password.
 * This is particularly useful when the user requests a password reset via email,
 * and after logging in with the new temporary password, this activity can be
 * used to change the password to a permanent one.
 */
public class SettingsActivity extends AppCompatActivity {

    private Button mPasswordResetButton;
    private EditText mPasswordFieldOne;
    private EditText mPasswordFieldTwo;
    private String mPasswordOne;
    private String mPasswordTwo;
    private String mLoginName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        initViews();
        mPasswordOne = "";
        mPasswordTwo = "";
        mLoginName = "";
    }

    protected void initViews() {
        mPasswordResetButton = (Button) findViewById(R.id.settings_changepwbutton);
        mPasswordFieldOne = (EditText) findViewById(R.id.settings_newpw1);
        mPasswordFieldTwo = (EditText) findViewById(R.id.settngs_newpw2);
    }

    public void processEvents(View view) {
        if(view == mPasswordResetButton) {
            mPasswordOne = mPasswordFieldOne.getText().toString();
            mPasswordTwo = mPasswordFieldTwo.getText().toString();
            if(mPasswordOne.length() == 0 || mPasswordTwo.length() == 0) {
                Toast.makeText(SettingsActivity.this,
                        "Please enter a password", Toast.LENGTH_SHORT).show();
            } else {
                if(mPasswordOne.equals(mPasswordTwo)) {
                    new WebConnect().execute("");
                } else {
                    Toast.makeText(SettingsActivity.this,
                            "Your passwords do not match", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    /**
     * Internal class to handle netwrok communication on a separate thread than the UI thread.
     */
    private class WebConnect extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return connectToServer();
        }

        protected HttpURLConnection getConnection() throws IOException
        {
            URL url = new URL("http://autsdp2016.com/newpassword.php?password=" + mPasswordOne
                    + "&username=" + getSharedPreferences("loginPrefs", MODE_PRIVATE).getString("name", "EMPTY"));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[1];
            reader.read(buffer);
            String s = new String(buffer);
            Log.d("response", s);
            if (s.contains("0"))
                return "Reset failed";
            else {
                return "Reset success";
            }
        }

        protected String connectToServer() {
            String s = "";
            try {
                HttpURLConnection conn = getConnection();
                final String result = getResponseFromServer(conn);
                runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setContentView(R.layout.settings);
                    initViews();
                    if(result.equals("Reset success")) {
                        Toast.makeText(SettingsActivity.this,
                                "Your password has been set!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SettingsActivity.this,
                                "An error occurred, please try again later", Toast.LENGTH_SHORT).show();
                    }
                }
                });
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setContentView(R.layout.settings);
                        initViews();
                        Toast.makeText(SettingsActivity.this,
                                "An error occurred, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return s;
        }
    }
}
