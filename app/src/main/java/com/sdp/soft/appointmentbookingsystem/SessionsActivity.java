package com.sdp.soft.appointmentbookingsystem;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.sdp.soft.appointmentbookingsystem.Controllers.NetworkController;
import com.sdp.soft.appointmentbookingsystem.Models.Coach;
import com.sdp.soft.appointmentbookingsystem.Models.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.ListResourceBundle;

/**
 * This Activity will display a dropdown list containing all the availalbe coaches,
 * and a list of available sessions for each coach.
 * The user can swtich between different coaches and the displayed sessions will be
 * changed accordingly.
 * When a session is clicked it will be booked and removed from the list.
 */
public class SessionsActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private ListView sessionsListView;
    private TextView noSessionsLabel;
    private Spinner coachSpinner;

    private SessionAdapter sessionsAdapter;
    private CoachAdapter coacheAdapter;
    private List<Coach> coaches;
    private List<Session> sessions;

    //the date selected by the user on the calendar (from the calendar activity)
    private static String selectedDate;
    private static int sessionToBookId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);

        progressBar = (ProgressBar) findViewById(R.id.sessions_loading_bar);
        progressBar.setVisibility(View.GONE);

        initViews();

        //extract the date chosen from the calendar from the Calendar Activity.
        Intent intent = getIntent();
        selectedDate = intent.getStringExtra("date");

        //set up the list of coaches.
        try {
            coaches = new ArrayList<>(2);
            getCoaches();
            coachSpinner = (Spinner) findViewById(R.id.planets_spinner);
            coacheAdapter = new CoachAdapter(this, coaches);
            coachSpinner.setAdapter(coacheAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        coachSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView adapter, View v, int index, long lng) {
                progressBar.setVisibility(View.VISIBLE);
                sessions = new ArrayList<>();
                int id = coaches.get(index).getCoachId();
                getSessionsForCoach(id, selectedDate);
                sessionsAdapter.mSessions = sessions;
                sessionsAdapter.notifyDataSetChanged();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView)
            {}
        });

        //set up the list of sessions
        sessions = new ArrayList<>(3);
        sessionsAdapter = new SessionAdapter(this, sessions);
        sessionsListView = (ListView) findViewById(R.id.sessions_list);
        sessionsListView.setAdapter(sessionsAdapter);
        sessionsListView.setFastScrollEnabled(true);

        sessionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id){
                Session session = sessions.get(position);
                bookSession(session);
                Toast.makeText(SessionsActivity.this,
                        "Session has been booked successfully", Toast.LENGTH_SHORT).show();
                showNotification();
            }
        });
    }

    private void initViews() {
        noSessionsLabel = (TextView) findViewById(R.id.no_sessions_label);
    }

    /**
     * Book a specific session by sending the user id and session id
     * to the server.
     * @param session: the session selected by the user from the sessions list view.
     */
    private void bookSession(Session session) {

        URL  url = null;
        try {
            url = new URL("http://autsdp2016.com/booksession.php?sessionid="+session.getSessionId()+ "&userid=" + getSharedPreferences("loginPrefs", MODE_PRIVATE).getString("userId", "0"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        //get the session id of the session to be booked.
        sessionToBookId = session.getSessionId();
        new WebConnect().execute(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }

    /**
     * An adapter class that acts as a data source for the coaches spinner (dropdown list).
     */
    public class CoachAdapter extends BaseAdapter {
        Context mContext;
        List<Coach> mCoaches;
        LayoutInflater inflater;

        public CoachAdapter(Context context, List<Coach> coaches) {
            mContext = context;
            mCoaches = coaches;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mCoaches.size();
        }

        @Override
        public Coach getItem(int position) {
            return mCoaches.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mCoaches.get(position).getCoachId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (convertView == null)
                rowView = inflater.inflate(R.layout.spinner_item, parent, false);

            TextView textView = (TextView) rowView.findViewById(R.id.spinner_item_text);

            Coach coach = getItem(position);
            try {
                textView.setText(coach.getFullName());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return rowView;
        }
    }

    /**
     * An adapter class that acts as a data source for the sessions list view.
     */
    private class SessionAdapter extends BaseAdapter {
        Context mContext;
        List<Session> mSessions;
        LayoutInflater inflater;

        public SessionAdapter(Context context, List<Session> sessions) {
            mContext = context;
            mSessions = sessions;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void removeSessionById (int sessionId) {
            Iterator<Session> sessionIterator = mSessions.iterator();

            while (sessionIterator.hasNext()){
                if (sessionIterator.next().getSessionId() == sessionId) {
                    sessionIterator.remove();
                }
            }
        }

        @Override
        public int getCount() {
            return mSessions.size();
        }

        @Override
        public Session getItem(int position) {
            return mSessions.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mSessions.get(position).getSessionId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (convertView == null)
                rowView = inflater.inflate(R.layout.spinner_item, parent, false);//spinner_item

            TextView textView = (TextView) rowView.findViewById(R.id.spinner_item_text);//spinner_item_text

            Session session = getItem(position);
            try {
                textView.setText(session.sessionStartFinishTimes());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return rowView;
        }
    }

    /**
     * Retrieves a list of coaches from the server, this list will used to populate
     * a spinner (dropdown list) with the names of the coaches.
     */
    public void getCoaches() {
        // Tag used to cancel the request
        String tag_json_arry = "json_array_req";

        String url = "http://autsdp2016.com/coaches.php";

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        VolleyLog.d("Error: " + response.toString());
                        try{
                            for (int i=0; i<response.length(); i++) {
                                JSONObject coach = response.getJSONObject(i);
                                coaches.add(new Coach(coach.getString("name"), coach.getString("name"), Integer.parseInt(coach.getString("coach_id"))));
                                coacheAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
            }
        });

        // Adding request to request queue
        NetworkController.getInstance().addToRequestQueue(req, tag_json_arry);
    }

    /**
     * Retrieves a list of available sessions for a specific coach from the server based on the
     * date selected by the user.
     * @param coachId: The id of the coach for whom sessions will be fetched.
     * @param selectedDate: the date selected by the user.
     */
    public void getSessionsForCoach(int coachId, String selectedDate) {

        // Tag used to cancel the request
        String tag_json_arry = "json_array_req";

        String url = "http://autsdp2016.com/sessions.php?coach_id="+coachId+"&date="+selectedDate;

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Success ", response.toString());
                        sessions.clear();

                        if(response.length() == 0){
                            //display a message to indicate that there are no sessions availabe on that day.
                            noSessionsLabel.setVisibility(View.VISIBLE);
                        } else {
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject session = response.getJSONObject(i);
                                    String startTime = session.getString("time").substring(0, session.getString("time").indexOf('-'));
                                    String finishTime = session.getString("time").substring(session.getString("time").indexOf('-') + 1, session.getString("time").length());

                                    sessions.add(new Session(startTime, finishTime, Integer.parseInt(session.getString("session"))));
                                    sessionsAdapter.notifyDataSetChanged();
                                }
                                if (sessionsAdapter.mSessions.size() != 0) {
                                    noSessionsLabel.setVisibility(View.GONE);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    progressBar.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
            }
        });
        // Adding request to request queue
        NetworkController.getInstance().addToRequestQueue(req, tag_json_arry);
    }

    /**
     * An internal class to handle network communication on a separate thread than
     * the UI thread.
     */
    private class WebConnect extends AsyncTask<URL, Void, String> {
        protected String doInBackground(URL... urls) {
            if(urls.length == 0 || urls == null) {
                return "No url specified";
            }
            return connectToServer(urls[0]);
        }

        protected HttpURLConnection getConnection(URL url) throws IOException {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[1];
            reader.read(buffer);
            String s = new String(buffer);
            Log.d("server:",s);
            if (s.contains("0"))
                return "Error Booking Session";
            else {
                return "Success!";
            }
        }

        protected String connectToServer(URL url) {
            String s = "";
            try {
                HttpURLConnection conn = getConnection(url);
                String result = getResponseFromServer(conn);
                Log.d("response", result);
                if(result.equalsIgnoreCase("Success!")) {
                    removeBookedSessionFromList(sessionToBookId);
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SessionsActivity.this,
                                    "Booking Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(SessionsActivity.this,
                                "An error occured, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return s;
        }

        private void removeBookedSessionFromList(int sessionId) {
            sessionsAdapter.removeSessionById(sessionId);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sessionsAdapter.notifyDataSetChanged();
                }
            });

        }
    }

    /**
     *  The showNotification method creates a notification and sends the user to the book session activity
     *  when the notification is clicked outside the UI
     */
    private void showNotification () {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, BookedSessionsActivity.class), 0);
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("Elite Trainer")
                .setSmallIcon(R.drawable.main1)
                .setContentTitle("Elite Trainer")
                .setContentText("Training Reminder")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }


}
