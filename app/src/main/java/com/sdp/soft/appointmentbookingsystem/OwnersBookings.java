package com.sdp.soft.appointmentbookingsystem;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.sdp.soft.appointmentbookingsystem.Controllers.NetworkController;
import com.sdp.soft.appointmentbookingsystem.Models.Coach;
import com.sdp.soft.appointmentbookingsystem.Models.Session;
import com.sdp.soft.appointmentbookingsystem.Utls.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OwnersBookings extends AppCompatActivity {

    private Map<String, Session> oBookedSessions;
    private ListView oBookedSessionsListView;
    private BookedSessionAdapter oBookedSessionAdapter;
    private TextView noBookedSessionsTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owners_bookings);

        initViews();


        oBookedSessions = new HashMap<String, Session>(3);
        oBookedSessionAdapter = new BookedSessionAdapter(oBookedSessions);
        oBookedSessionsListView = (ListView) findViewById(R.id.booked_sessions_list);
        oBookedSessionsListView.setAdapter(oBookedSessionAdapter);
        oBookedSessionsListView.setFastScrollEnabled(true);

        getAllSessionsForOwner();
    }
    private void initViews(){
        noBookedSessionsTextView = (TextView) findViewById(R.id.no_booked_sessions_text_view);
    }

    private void getAllSessionsForOwner (){


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Fetching Booked Sessions...");
        pDialog.show();

        // Tag used to cancel the request
        String tag_json_arry = "json_array_req";

        //String url = "http://autsdp2016.com/bookedsessions.php?userId="+userId;

        String url = "http://autsdp2016.com/ownersessions.php";

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("Success ", response.toString());
                        pDialog.hide();
                        oBookedSessions.clear();

                        if(response.length() == 0){
                            // a temporary hack to make sure that something is displayed if there are no sessions available
                            //mBookedSessions.add(new Session("No sessions available!", "", -1));
                            //mBookedSessionAdapter.notifyDataSetChanged();
                            //TODO ADD CODE TO HANDLE THE CASE WHERE THERE ARE NO SESSIONS

                        } else {
                            try {
                                for (int i = 0; i < response.length(); i++) {
                                    JSONObject responseObject = response.getJSONObject(i);

                                    Session session = StringUtils.extractSessionDetailsFromJsonObject(responseObject);
                                    String coachName = responseObject.getString("coachname");

                                    oBookedSessions.put(coachName, session);
                                }
                                oBookedSessionAdapter.oBookedSessionsList.clear();
                                oBookedSessionAdapter.updateAdapterData(oBookedSessions);
                                oBookedSessionAdapter.notifyDataSetChanged();

                                System.out.print("");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
            }
        });
        // Adding request to request queue
        NetworkController.getInstance().addToRequestQueue(req, tag_json_arry);
    }

    private class BookedSessionAdapter extends BaseAdapter {
        private ArrayList oBookedSessionsList;

        public BookedSessionAdapter(Map<String, Session> mBookedSessionsMap) {
            oBookedSessionsList = new ArrayList();
            oBookedSessionsList.addAll(mBookedSessionsMap.entrySet());
        }

        public void updateAdapterData(Map<String, Session> mBookedSessionsMap) {
            oBookedSessionsList = new ArrayList();
            oBookedSessionsList.addAll(mBookedSessionsMap.entrySet());
        }

        @Override
        public int getCount() {
            return oBookedSessions.size();
        }

        @Override
        public Map.Entry<String, Session> getItem(int position) {
            return (Map.Entry) oBookedSessionsList.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO implement logic with ID
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View result = LayoutInflater.from(parent.getContext()).inflate(R.layout.booked_session_list_item, parent, false);
            Map.Entry<String, Session> item = getItem(position);

            String sessionDetails = StringUtils.getWeekdayName(item.getValue().getSessionDate()) + " " +
                    item.getValue().getSessionDate() + "\n" + item.getValue().sessionStartFinishTimes();
            String coachDetails = "With " + item.getKey();

            TextView sessionTextView = (TextView) result.findViewById(R.id.text1);
            TextView coachTextView = (TextView) result.findViewById(R.id.text2);

            sessionTextView.setText(sessionDetails);
            coachTextView.setText(coachDetails);

            return result;
        }
    }
}
