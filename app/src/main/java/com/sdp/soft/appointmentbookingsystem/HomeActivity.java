package com.sdp.soft.appointmentbookingsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private Button home_logoutbutton;
    private Button home_sessionsbutton;
    private Button home_viewsessionsbutton;
    private Button home_ownersessionsbutton;
    private Button home_blocksessionsbutton;
    private TextView home_label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        home_logoutbutton = (Button)findViewById(R.id.home_logoutbutton);
        home_sessionsbutton = (Button)findViewById(R.id.home_viewcoachesbutton);
        home_viewsessionsbutton = (Button) findViewById(R.id.home_viewsessionsbutton);
        home_ownersessionsbutton = (Button) findViewById(R.id.home_viewownersessionsbutton);
        home_blocksessionsbutton = (Button) findViewById(R.id.home_coachblockbutton);
        home_label = (TextView)findViewById(R.id.home_title);
        SharedPreferences sp = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        String name = sp.getString("name", " ");

        String userId = getSharedPreferences("loginPrefs", MODE_PRIVATE).getString("userId", "0");
        int ownerCheck = Integer.parseInt(userId);
        if (ownerCheck != 1) {
            home_ownersessionsbutton.setVisibility(View.GONE);
        }

        home_label.setText("Hi there, " + name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }

    public void processEvents(View view){
        if(view==home_logoutbutton) {
            SharedPreferences sp = getSharedPreferences("loginPrefs", MODE_PRIVATE);
            SharedPreferences.Editor ed = sp.edit();
            ed.clear();
            ed.commit();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if(view==home_sessionsbutton) {
            Intent intent = new Intent(this, CalenderActivity.class);
            startActivity(intent);
        } else  if(view == home_viewsessionsbutton) {
            Intent intent = new Intent(this, BookedSessionsActivity.class);
            startActivity(intent);
        }else  if(view == home_ownersessionsbutton) {
            Intent intent = new Intent(this, OwnersBookings.class);
            startActivity(intent);
        } else if(view == home_blocksessionsbutton) {
            getSharedPreferences("coachUserConfigs", MODE_PRIVATE).edit().putString("bookOrBlock", "BLOCK").commit();
            Intent intent = new Intent(this, CalenderActivity.class);
            startActivity(intent);
        }

    }
}
