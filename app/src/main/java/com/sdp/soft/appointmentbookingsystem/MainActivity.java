package com.sdp.soft.appointmentbookingsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

/**
 * This activity represents the main entry point for the application.
 * It checks if the user details are stored in shared preferences (i.e if the user has previously
 * selected the "keep me logged in option" during the last log in.
 * If no details are stored, it will launch the Login Activity otherwise, it will launch the Home
 * Activity.
 */
public class MainActivity extends AppCompatActivity {

    private SharedPreferences loginPreferences;
    private Boolean saveLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            Intent launchActivity = new Intent(this, HomeActivity.class);
            startActivity(launchActivity);
            finish();
        } else {
            Intent launchActivity = new Intent(this, LoginActivity.class);
            startActivity(launchActivity);
            finish();
        }
    }
}
