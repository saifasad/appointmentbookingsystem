package com.sdp.soft.appointmentbookingsystem;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.sdp.soft.appointmentbookingsystem.Controllers.NetworkController;
import com.sdp.soft.appointmentbookingsystem.Models.Session;
import com.sdp.soft.appointmentbookingsystem.Utls.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BlockSessionsActivity extends AppCompatActivity {

    private String date = "";
    private String userid = "";
    private Button requestSessionsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_sessions);
        Intent intent = getIntent();
        date = intent.getStringExtra("date");
        Log.d("DATE", date);
        userid = getSharedPreferences("loginPrefs", MODE_PRIVATE).getString("userId", "0");
        Log.d("userid", userid);
        requestSessionsButton = (Button) findViewById(R.id.block_getsesions);
    }

    public void processEvents(View view) {
        if(view == requestSessionsButton) {
            new WebConnect().execute();
        }
    }

    private class WebConnect extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return connectToServer();
        }

        protected HttpURLConnection getConnection() throws IOException
        {
            URL url = new URL("http://autsdp2016.com/coachfreesessions.php?userid="+userid+"&date="+date);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[1];
            reader.read(buffer);
            String s = new String(buffer);
            //String response = s.split("F")[0];
            Log.d("server:",s);
            if (s.contains("1"))
                return "Records Added";
            else {
                return "Error";
            }
        }

        protected String connectToServer() {
            String s = "";
            try {
                HttpURLConnection conn = getConnection();
                String result = getResponseFromServer(conn);
                Log.d("response", result);
                if(result.equals("Records Added")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(BlockSessionsActivity.this,
                                    "Records have been added!", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(BlockSessionsActivity.this,
                                    "No records added", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(BlockSessionsActivity.this,
                                "An error occured, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            return s;
        }
    }
}
