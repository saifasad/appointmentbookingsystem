package com.sdp.soft.appointmentbookingsystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.util.Date;
import static com.sdp.soft.appointmentbookingsystem.Utls.StringUtils.exractDateFromString;


/**
 * This activity displays a calendar, to allow the user to select a certain date to book
 * a session, it only allows certain days to be selected.
 */
public class CalenderActivity extends AppCompatActivity implements OnDateSelectedListener {

    //will hold a refernece to the calendar view
    MaterialCalendarView materialCalendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);

        materialCalendarView = (MaterialCalendarView) findViewById(R.id.calendarView);

        materialCalendarView.setOnDateChangedListener(this);

        //setup the calender to only allow selection of dates starting from the current date
        //and set the calendar mode to months.
        materialCalendarView.state().edit()
                .setMinimumDate(new Date())
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }

    /**
     * When a date is selected on the calender, this method is executed.
     * It will laucnh the SessionsActivty to handle display the sessions
     * availavle on the selected date.
     *
     * @param widget the calender view.
     * @param date: the date selected by the user on the calendar.
     * @param selected: indicates that a date has been selected.
     */
    @Override
    public void onDateSelected(MaterialCalendarView widget,
                               CalendarDay date,
                               boolean selected) {

        String selectedDate = exractDateFromString(date.toString());
        String blockOrBook = getSharedPreferences("coachUserConfigs", MODE_PRIVATE).getString("bookOrBlock", "BOOK");
        Intent intent;
        if(blockOrBook.equalsIgnoreCase("BOOK")) {
            intent = new Intent(this, SessionsActivity.class);
        } else {
            intent = new Intent(this, BlockSessionsActivity.class);
            getSharedPreferences("coachUserConfigs", MODE_PRIVATE).edit().putString("bookOrBlock","BOOK").commit();
        }
        String extraMessage = "date";
        intent.putExtra(extraMessage, selectedDate);
        startActivity(intent);
    }

}
