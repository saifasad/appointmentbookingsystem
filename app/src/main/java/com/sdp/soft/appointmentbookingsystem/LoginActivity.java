package com.sdp.soft.appointmentbookingsystem;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private Button mLoginButton, mRegisterButton, mPasswordResetButton;
    private EditText mPasswordField, mNameField;
    private TextView mLoadingLabel;
    private String mName, mPassword;
    private CheckBox mLoginCheckBox;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;
    private static String SAVED_LOGIN = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        initViews();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    //    getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        Retrieve the login name from RegistrationActivity.java
        This will only execute if the user has successfully registered
        prior to LoginActivity being launched
        */
        String loginFromRegistrationActivity = RegistrationActivity.LOGIN_NAME;
        String loginFromRecoveryActivity = RecoveryActivity.LOGIN_NAME;
        try {
            if (!loginFromRegistrationActivity.equals("%"))
                mNameField.setText(loginFromRegistrationActivity);
            else if(!SAVED_LOGIN.equals(""))
                mNameField.setText(mName);
            if(!loginFromRecoveryActivity.equals("%"))
                mNameField.setText(loginFromRecoveryActivity);
        } catch(NullPointerException e) {
            /*
            Do nothing, this exception will occur if RegistrationActivity
            hasn't been launched
            */
        }
    }

    public void initViews() {
        mLoginButton = (Button) findViewById(R.id.login_loginbutton);
        mRegisterButton = (Button) findViewById(R.id.login_registerbutton);
        mPasswordResetButton = (Button) findViewById(R.id.login_passwordbutton);
        mPasswordField = (EditText) findViewById(R.id.login_passwordfield);
        mNameField = (EditText) findViewById(R.id.login_loginfield);
        mLoginCheckBox = (CheckBox) findViewById(R.id.login_logincheckbox);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin) {
            mNameField.setText(loginPreferences.getString("NAME_KEY", ""));
            mPasswordField.setText(loginPreferences.getString("PASSWORD_KEY", ""));
            mLoginCheckBox.setChecked(true);
        }
    }

    public void processEvents(View view) {
        if(view == mLoginButton) {
            InputMethodManager x = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            x.hideSoftInputFromWindow(mNameField.getWindowToken(), 0);
            mName = mNameField.getText().toString();
            SAVED_LOGIN = mName;
            mPassword = mPasswordField.getText().toString();
            if (mLoginCheckBox.isChecked()) {
                loginPrefsEditor.putBoolean("saveLogin", true);
            }
            loginPrefsEditor.putString("name", mName);
            loginPrefsEditor.commit();
            setContentView(R.layout.loading_layout);
            mLoadingLabel = (TextView) findViewById(R.id.loading_title);
            mLoadingLabel.setText("Logging you in...");
            new WebConnect().execute("");
        }
        if(view == mRegisterButton) {
            Intent launchActivity = new Intent(this, RegistrationActivity.class);
            startActivity(launchActivity);
        }
        if(view == mPasswordResetButton) {
            Intent launchActivity = new Intent(this, RecoveryActivity.class);
            startActivity(launchActivity);
        }
    }

    protected void launchHomeActivity() {
        Intent launchActivity = new Intent(this, HomeActivity.class);
        startActivity(launchActivity);
        finish();
    }

    private class WebConnect extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return connectToServer();
        }

        protected HttpURLConnection getConnection() throws IOException
        {
            URL url = new URL("http://autsdp2016.com/login.php?login=" + mName + "&password=" + mPassword);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[10];
            reader.read(buffer);
            String s = new String(buffer);
            String response = s.split("F")[0];
            Log.d("server:",s);
            if (response.equals("0"))
                return "Incorrect username or password";
            else {
                loginPrefsEditor.putString("userId", response).commit();
                return "Login Success!";
            }
        }

        protected String connectToServer() {
            String s = "";
            try {
                HttpURLConnection conn = getConnection();
                String result = getResponseFromServer(conn);
                Log.d("response", result);
                if(result.equals("Login Success!")) {
                    launchHomeActivity();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setContentView(R.layout.login);
                            initViews();
                            Toast.makeText(LoginActivity.this,
                                    "Your username or password is incorrect", Toast.LENGTH_SHORT).show();
                            mNameField.setText(mName);
                        }
                    });
                }
            } catch (IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setContentView(R.layout.login);
                        initViews();
                        Toast.makeText(LoginActivity.this,
                                "An error occured, please try again", Toast.LENGTH_SHORT).show();
                        mNameField.setText(mName);
                    }
                });
            }
            return s;
        }
    }
}
