package com.sdp.soft.appointmentbookingsystem;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class RecoveryActivity extends AppCompatActivity {

    private Button mRequestButton;
    private EditText mEmailText;
    private EditText mLoginText;
    private ProgressBar mProgressBar;
    private String mEmailString, mLoginString;
    public static String LOGIN_NAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery);
        initViews();
        mEmailString = "";
        mLoginString = "";
        LOGIN_NAME = "%";
    }

    protected void initViews() {
        mRequestButton = (Button) findViewById(R.id.recovery_requestbutton);
        mEmailText = (EditText) findViewById(R.id.recovery_emailfield);
        mLoginText = (EditText) findViewById(R.id.recovery_loginfield);
        mProgressBar = (ProgressBar) findViewById(R.id.recovery_loadingbar);
        mProgressBar.setVisibility(View.GONE);
    }

    public void processEvents(View view) {
        if(view == mRequestButton) {
            mEmailString = mEmailText.getText().toString();
            mLoginString = mLoginText.getText().toString();
            mProgressBar.setVisibility(View.VISIBLE);
            mRequestButton.setVisibility(View.GONE);
            LOGIN_NAME = mLoginString;
            new WebConnect().execute("");
        }
    }

    private class WebConnect extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return connectToServer();
        }

        protected HttpURLConnection getConnection() throws IOException
        {
            URL url = new URL("http://autsdp2016.com/recoverpassword.php?email=" + mEmailString
             + "&username=" + mLoginString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[1];
            reader.read(buffer);
            String s = new String(buffer);
            if (s.equals("0"))
                return "Email Not Found";
            else if (s.equals("1")){
                return "Email Sent";
            } else {
                return "Email Not Valid";
            }
        }

        protected String connectToServer() {
            String s = "";
            try {
                HttpURLConnection conn = getConnection();
                final String result = getResponseFromServer(conn);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.GONE);
                        mRequestButton.setVisibility(View.VISIBLE);
                        if(result.equals("Email Sent")) {
                            Toast.makeText(RecoveryActivity.this,
                                    "Email sent! Check your spam folder if you don't receive it",
                                    Toast.LENGTH_LONG).show();
                        } else if(result.equals("Email Not Valid")) {
                            Toast.makeText(RecoveryActivity.this,
                                    "The email you entered is not valid. Check your formatting and try again",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RecoveryActivity.this,
                                    "The email or username you entered is not registered with us, please try again",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } catch (IOException e) {
                Log.e("Error:", e.getMessage());
            }
            return s;
        }
    }
}
