package com.sdp.soft.appointmentbookingsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class RegistrationActivity  extends AppCompatActivity  {

    private Button rButton;
    private EditText rPasswordField, rNameField, rLoginField, rAddressField, rPhoneField, rEmailField;
    private CheckBox rIsCoachBox;
    private TextView rLabel;
    private String rName, rPassword, rAddress, rPhoneNumber, rEmail, rCoach, rLogin;
    public static String LOGIN_NAME;
    SharedPreferences sharedPrefs;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        initViews();
        sharedPrefs = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        editor = sharedPrefs.edit();
        // Initialize as % indicating this string hasn't been saved
        LOGIN_NAME = "%";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        return true;
    }

    public void initViews() {
        rButton = (Button)findViewById(R.id.reg_registerbutton);
        rPasswordField = (EditText)findViewById(R.id.reg_passwordfield);
        rNameField = (EditText)findViewById(R.id.reg_namefield);
        rLoginField = (EditText)findViewById(R.id.reg_usernamefield);
        rAddressField = (EditText)findViewById(R.id.reg_addressfield);
        rPhoneField = (EditText)findViewById(R.id.reg_phonefield);
        rEmailField = (EditText)findViewById(R.id.reg_emailfield);
        rIsCoachBox = (CheckBox)findViewById(R.id.reg_coachcheckbox);
        rLabel = (TextView)findViewById(R.id.reg_title);
    }

    public void ProcessEvents(View view) {
        if(view == rButton) {
            rName = rNameField.getText().toString();
            rName = rName.replace(" ", "%20");
            rLogin = rLoginField.getText().toString();
            rPassword = rPasswordField.getText().toString();
            rAddress = rAddressField.getText().toString();
            rPhoneNumber = rPhoneField.getText().toString();
            rEmail = rEmailField.getText().toString();
            if(rIsCoachBox.isChecked()) {
                rCoach = "y";
            }
            else {
                rCoach = "n";
            }
            setContentView(R.layout.loading_layout);
            TextView loadingLabel = (TextView) findViewById(R.id.loading_title);
            loadingLabel.setText("Registering your account...");
            editor.putString("name", rName);
            new RegConnect().execute("");
        }
    }

    private class RegConnect extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... urls) {
            return connectToServer();
        }

        protected HttpURLConnection getConnection() throws IOException
        {
            String urlString = "http://autsdp2016.com/register.php?name=" + rName + "&password=" + rPassword
                    + "&email=" + rEmail + "&phone=" + rPhoneNumber + "&address=" + rAddress + "&coach=" + rCoach
                    + "&login=" + rLogin;
            URLEncoder.encode(urlString, "UTF-8");
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn;
        }

        protected String getResponseFromServer(HttpURLConnection conn) throws IOException {
            InputStream is = conn.getInputStream();
            Reader reader = new InputStreamReader(is, "UTF-8");
            char[] buffer = new char[1];
            reader.read(buffer);
            String s = new String(buffer);
            if (s.equals("0"))
                return "Error encountered during registration";
            else
                return "Registration Successful!";
        }

        protected String connectToServer() {
            String s = "";
            try {
                HttpURLConnection conn = getConnection();
                final String result = getResponseFromServer(conn);
                if(result.equals("Registration Successful!")) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(RegistrationActivity.this,
                                    "Registration successful!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    LOGIN_NAME = rLogin;
                    finish();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setContentView(R.layout.registration);
                            initViews();
                            Toast.makeText(RegistrationActivity.this,
                                    "Registration failed, ensure you entered the correct information", Toast.LENGTH_SHORT).show();
                            rLoginField.setText(rLogin);
                            rNameField.setText(rName);
                            rAddressField.setText(rAddress);
                            rEmailField.setText(rEmail);
                            rPhoneField.setText(rPhoneNumber);
                            rPasswordField.setText(rPassword);
                            if(rCoach.equals("y"))
                                rIsCoachBox.setChecked(true);
                        }
                    });
                }
                rLabel.post(new Runnable() {
                    public void run() {
                        rLabel.setText(result);
                    }
                });
            } catch (IOException e) {
                Log.d("Error", e.getMessage());
            }
            return s;
        }
    }
}