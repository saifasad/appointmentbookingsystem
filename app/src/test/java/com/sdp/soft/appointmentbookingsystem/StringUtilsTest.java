package com.sdp.soft.appointmentbookingsystem;


import org.junit.Before;
import org.junit.Test;

import static com.sdp.soft.appointmentbookingsystem.Utls.StringUtils.exractDateFromString;
import static com.sdp.soft.appointmentbookingsystem.Utls.StringUtils.getWeekdayName;
import static org.junit.Assert.*;

/**
 * @Author Saif Asad on 26-Sep-16.
 *
 * This class contains unit tests for the StringUtils Class.
 */
public class StringUtilsTest {

    String expectedDate;
    String testDate;
    String weekdayNameTest;

    @Before
    public void setUp() throws Exception {

        //please note the test provided here shows the month is different, this is again due to the
        //issue with the calender that is returning the month before the selected month
        //the purpose of this test is to make sure that the date is extracted from the string correctly
        expectedDate = "2016-11-18";
        testDate = "CalendarDay{2016-10-18}";
        weekdayNameTest = "2016-10-19";

    }

    @Test
    public void date_is_extracted_in_the_correct_format() throws Exception {
        assertEquals(expectedDate, exractDateFromString(testDate));
    }

    @Test
    public void date_is_extracted_is_not_equal_to_the_input_date() throws Exception {
        assertNotEquals(testDate, exractDateFromString(testDate));
    }


    //----------------------------------------------------------------------------------------------
    //sprint 2 unit tests
    @Test
    public void weekday_name_is_extracted_correctly() throws Exception {
        String weekDay = "Wednesday";
        assertEquals(weekDay, getWeekdayName(weekdayNameTest));
    }

    @Test
    public void weekday_name_is_empty_empty_date_provided() throws Exception {
        String weekDay = "";
        weekdayNameTest = "";
        assertEquals(weekDay, getWeekdayName(weekdayNameTest));
    }

    @Test
    public void weekday_name_is_empty_wrong_date_format_provided() throws Exception {
        String weekDay = "";
        weekdayNameTest = "asfsdgsfr64689878";
        assertEquals(weekDay, getWeekdayName(weekdayNameTest));
    }
}
